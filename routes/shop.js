// shop.js

var mongo  = require('mongodb');

var Server = mongo.Server, 
    Db     = mongo.Db,
    BSON   = mongo.BSONPure;

var server = new Server('localhost', 27017, {auto_recorrect: true});
db         = new Db('shopdb', server, {safe: true});

db.open(function (err, db) {
    if (!err) {
        console.log("Connected to 'shopdb' database");
        db.collection('shops' ,{safe:true}, function(err, collection) {
            if (err) {
                console.log("The 'shops' collection doesn't exist. Creating it with sample data...");
                populateDB();
            }

        });
    }

});


exports.findById = function(req, res) {
    var id = req.params.id;
    console.log('retriving shop: '+ id);
    db.collection('shops', function(err, collection) {
        collection.findOne({'_id':new BSON.ObjectID(id)}, function(err, item) {
            res.send(item);
        });
    });
};

exports.findAll = function(req, res) {
    db.collection('shops', function(err, collection) {
        collection.find().toArray(function (err, items) {
            res.send(items);
        });
    });
};
                                  

exports.addShop = function(req, res) {
    var shop = req.body;
    console.log('Adding shop: ' + JSON.stringify(shop));
    db.collection('shops', function(err, collection) {
        collection.insert(shop, {safe:true}, function(err, result) {
            if(err) {
                res.send({'error':'An error has accurred'});
            } else {
                console.log('Success: ' + JSON.stringify(result[0]));
                res.send(result[0]);
            }
        });
   });
}

exports.updateShop = function(req, res) {
    var id = req.params.id;
    var shop = req.body;
    delete shop._id;
    console.log('Updating shop: ' + id);
    console.log(JSON.stringify(shop));
    db.collection('shops', function(err, collection) {
    collection.update({'_id':new BSON.ObjectID(id)}, shop, {safe:true}, function(err, result) {
        if (err) {
            console.log('Error udpdating shop: ' + err)
            res.send({'error': 'An error has occured'});
        } else {
            console.log('' + result + 'document(s) updated');
            res.send(shop);
        }
     }); 
   });
}


exports.deleteShop = function(req, res) {
    var id = req.params.id;
    console.log('Deleting shop: ' + id);
    db.collection('shops', function(err, collection) {
        collection.remove({'_id': new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
            if(err) {
                res.send({'eror':'An error has occured - ' + err});
            } else {
                console.log('' + result + 'document(s) deleted');
                res.send(req.body);

            }
        });
    });
}



// after this the database gets populated...

var populateDB  = function(){
    

    var shops = [
        {
            name: "vinky beauty collection",
            type: "beauty stuff",
            country: "Kenya",
            region: "nairobi",
            description: "we have no idea what this shop sells, infact we are very suspicous",
            photo: "saint_cosme.jpg"
        },
        {
            name: "Oloo trench coats",
            type: "clothware",
            country: "Kenya",
            region: "nairobi",
            description: "they say they sell trenchcoats but we've never seen one, one theory is they actually sell chicken feed",
            photo: "lan_rioja.jpg"
        }];
    
    
        db.collection('shops', function(err, collection) {
            collection.insert(shops, {safe:true}, function(err, result) {});
        });
};
            
