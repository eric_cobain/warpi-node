// shops.js

exports.findAll = function(req, res) {
    res.send([{name: "shop1"}, {name: "shop2"}, {name: "shop3"}]);
};

exports.findById = function(req, res) {
    res.send({id:req.param.id, name: "The name", description: "description"});
};
