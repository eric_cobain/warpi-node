// server.js

var express = require('express'),
    path = require('path'),
    http = require('http'),
    shop = require('./routes/shop');
    
var app = express();

app.configure(function () {
    app.set('port', process.env.PORT || 3000);
    app.use(express.logger('dev'));  /* 'default', 'short', 'tiny', 'dev' */
    app.use(express.bodyParser()),
    app.use(express.static(path.join(__dirname, 'public')));
}); 

app.get('/shops', shop.findAll);
app.get('/shops/:id', shop.findById);
app.post('/shops', shop.addShop);
app.put('/shops/:id', shop.updateShop);
app.delete('/shops/:id', shop.deleteShop);



http.createServer(app).listen(app.get('port'), function () {
    console.log("Express server listening on port " + app.get('port'));
});
