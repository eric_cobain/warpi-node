var AppRouter = Backbone.Router.extend({

    routes: {
        ""                  : "home",
        "shops"	: "list",
        "shops/page/:page"	: "list",
        "shops/add"         : "addShop",
        "shops/:id"         : "shopDetails",
        "about"             : "about"
    },

    initialize: function () {
        this.headerView = new HeaderView();
        $('.header').html(this.headerView.el);
    },

    home: function (id) {
        if (!this.homeView) {
            this.homeView = new HomeView();
        }
        $('#content').html(this.homeView.el);
        this.headerView.selectMenuItem('home-menu');
    },

	list: function(page) {
        var p = page ? parseInt(page, 10) : 1;
        var shopList = new ShopCollection();
        shopList.fetch({success: function(){
            $("#content").html(new ShopListView({model: shopList, page: p}).el);
        }});
        this.headerView.selectMenuItem('home-menu');
    },

    shopDetails: function (id) {
        var shop = new Shop({_id: id});
        shop.fetch({success: function(){
            $("#content").html(new ShopView({model: shop}).el);
        }});
        this.headerView.selectMenuItem();
    },

	addShop: function() {
        var shop = new Shop();
        $('#content').html(new ShopView({model: shop}).el);
        this.headerView.selectMenuItem('add-menu');
	},

    about: function () {
        if (!this.aboutView) {
            this.aboutView = new AboutView();
        }
        $('#content').html(this.aboutView.el);
        this.headerView.selectMenuItem('about-menu');
    }

});

utils.loadTemplate(['HomeView', 'HeaderView', 'ShopView', 'ShopListItemView', 'AboutView'], function() {
    app = new AppRouter();
    Backbone.history.start();
});
